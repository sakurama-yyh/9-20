package com.xmx.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xmx.mapper.GradeMapper;
import com.xmx.pojo.Grade;
import com.xmx.service.GradeService;
import org.springframework.stereotype.Service;

/**
* @author yangyuhang
* @description 针对表【grade】的数据库操作Service实现
* @createDate 2022-09-16 10:10:58
*/
@Service
public class GradeServiceImpl extends ServiceImpl<GradeMapper, Grade>
implements GradeService{

}
