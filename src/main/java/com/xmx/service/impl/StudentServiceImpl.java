package com.xmx.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xmx.mapper.StudentMapper;
import com.xmx.pojo.Student;
import com.xmx.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author yangyuhang
* @description 针对表【student】的数据库操作Service实现
* @createDate 2022-09-15 16:12:28
*/
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student>
implements StudentService{

    @Autowired
    StudentMapper studentMapper;

    @Override
    public PageInfo<Student> getList(int pageIndex, int pageSize) {
        PageHelper.startPage(pageIndex, 5);
        return new PageInfo<Student>(studentMapper.getList());
    }
}
