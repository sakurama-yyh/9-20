package com.xmx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xmx.pojo.Grade;

/**
* @author yangyuhang
* @description 针对表【grade】的数据库操作Service
* @createDate 2022-09-16 10:10:58
*/
public interface GradeService extends IService<Grade> {

}
