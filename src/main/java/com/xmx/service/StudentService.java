package com.xmx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.xmx.pojo.Student;

/**
* @author yangyuhang
* @description 针对表【student】的数据库操作Service
* @createDate 2022-09-15 16:12:28
*/
public interface StudentService extends IService<Student> {
    PageInfo<Student> getList(int pageIndex,int pageSize);
}
