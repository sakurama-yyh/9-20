package com.xmx.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * @TableName student
 */
@Data
public class Student implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId(value = "studentno",type = IdType.AUTO)
    private Integer studentno;
    /**
     *
     */
    private String studentname;
    /**
     *
     */
    private String loginpwd;
    /**
     *
     */
    private String sex;
    /**
     *
     */
    private Integer gradeid;
    /**
     *
     */
    private String phone;
    /**
     *
     */
    private String address;
    /**
     *
     */
    private String borndate;
    /**
     *
     */
    private String email;
    /**
     *
     */
    private String identitycard;
    @TableField(exist = false)
    private Grade grade;
}
