package com.xmx.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author yangyuhang
 * @TableName grade
 */
@Data
public class Grade implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId("gradeid")
    private Integer gradeid;
    /**
     *
     */
    private String gradename;
}
