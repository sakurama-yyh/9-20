package com.xmx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xmx.pojo.Grade;
import org.apache.ibatis.annotations.Mapper;

/**
* @author yangyuhang
* @description 针对表【grade】的数据库操作Mapper
* @createDate 2022-09-16 10:10:58
* @Entity com.xmx.pojo.Grade
*/
@Mapper
public interface GradeMapper extends BaseMapper<Grade> {
    Grade getGradeById(int id);

}
