package com.xmx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xmx.pojo.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
* @author yangyuhang
* @description 针对表【student】的数据库操作Mapper
* @createDate 2022-09-15 16:12:28
* @Entity com.xmx.pojo.Student
*/
@Mapper
public interface StudentMapper extends BaseMapper<Student> {
    List<Student> getList();

}
