package com.xmx.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.pagehelper.PageInfo;
import com.xmx.pojo.Student;
import com.xmx.service.GradeService;
import com.xmx.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author yangyuhang
 */
@RestController
@RequestMapping("student")
public class StudentController {

    @Autowired
    StudentService studentService;
    @Autowired
    GradeService gradeService;

    @RequestMapping(value = "list/{pageIndex}/{pageSize}",produces="application/json;charset=utf-8")
    public String list(@PathVariable("pageIndex") Integer pageIndex,@PathVariable("pageSize") Integer pageSize) {
        PageInfo<Student> list = studentService.getList(pageIndex, pageSize);
        return JSON.toJSONString(list, SerializerFeature.DisableCircularReferenceDetect);
    }

    @RequestMapping("del/{id}")
    public String del(@PathVariable("id") Integer id) {
        QueryWrapper<Student> qw = new QueryWrapper<>();
        qw.eq("studentno", id);
        boolean b = studentService.remove(qw);

        return JSON.toJSONString(b) + "";
    }

    @PostMapping("add")
    public String add(@RequestBody Student student) {
        boolean b = studentService.save(student);
        return JSON.toJSONString(b) + "";
    }

    @GetMapping("edit/{studentno}")
    public String edit(@PathVariable("studentno") Integer studentno) {
        Student student = studentService.getOne(new QueryWrapper<Student>().eq("studentno", studentno));
        return JSON.toJSONString(student);
    }

    @PutMapping("update")
    public String update(@RequestBody Student student) {
        UpdateWrapper<Student> uw = new UpdateWrapper<>();
        uw.eq("studentno",student.getStudentno());
        boolean b = studentService.update(student, uw);
        return JSON.toJSONString(b) + "";
    }

}
