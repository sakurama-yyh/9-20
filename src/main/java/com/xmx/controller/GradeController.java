package com.xmx.controller;

import com.alibaba.fastjson.JSON;
import com.xmx.pojo.Grade;
import com.xmx.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author yangyuhang
 */
@RestController
@RequestMapping("grade")
public class GradeController {
    @Autowired
    GradeService gradeService;

    @PostMapping("addGrade")
    public String addGrade(@RequestBody Grade grade) {
        boolean b = gradeService.save(grade);

        return JSON.toJSONString(b)+"";
    }

    @GetMapping("list")
    public String list() {
        List<Grade> list = gradeService.list();
        return JSON.toJSONString(list);
    }
}
