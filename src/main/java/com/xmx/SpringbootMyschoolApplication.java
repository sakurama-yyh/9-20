package com.xmx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yangyuhang
 */
@SpringBootApplication
public class SpringbootMyschoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMyschoolApplication.class, args);
    }

}
