package com.xmx;

import com.xmx.service.StudentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest
class SpringbootMyschoolApplicationTests {

    @Autowired
    StudentService studentService;
    @Test
    void contextLoads() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("sex","男");
        map.put("gradeid",1);
        System.out.println(studentService.listByMap(map));
    }

}
